* Contents of this directory:

- Code to make DAG to run the spin evolution and application of fits in a single step 

make_BBH_spin_evolution_and_fit_application_pipeline.py

- Code to apply the fits and append the posterior samples to the resulting posterior_samples.dat file (can be run as a stand-alone
  code on aligned-spin runs, where no spin evolution is required)

apply_fits_and_append_samples.py

- Other subsidiary scripts

automated_postproc_utils.py - Contains a function to print Git status info for LALSuite and o2-postprocessing, as well as numpy and scipy versions,
			      plus a function to compute the orbital velocty from the GW frequency

combine_and_append_evolved_posterior_samples_non-HDF5.py - Combine evolved spin posterior samples and append them to posterior_samples.dat file

evolve_spins.py - Evolve the spins using PN expressions

split_posterior_samples.py - Splits posterior_samples.dat file into many small files for parallelization

- to install these scripts (preferrably inside a venv):

python setup.py install

- Other

README.md - This file

* Top-level usage of the codes in this directory (i.e., just for the first two codes) without installation:

./make_BBH_spin_evolution_and_fit_application_pipeline.py --posfile (path of input posterior_samples.dat file)
  --ntasks (number of cores over which to parallelize)
  --rundir (directory for split sample files and other intermediate output)

./apply_fits_and_append_samples.py --posfile (path of input posterior_samples.dat file) --aligned