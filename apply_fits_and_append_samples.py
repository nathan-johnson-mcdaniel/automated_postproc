#!/usr/bin/env python

'''
This code reads in LALInference posterior samples and appends the samples for the final mass (both detector and source frame) and spin, radiated energy, and peak luminosity, using various fits, some of which include the effects of in-plane spins on the final spin, and averaging the fits to give final results

Usage:

./apply_fits_and_append_samples.py --posfile <path of input posterior samples file> [--aligned]

Use the --aligned option when running on aligned-spin posterior samples
'''
# NKJ-M, 5.2018, based on calc_all_post_O2a.py; final modifications by NKJ-M in 7.2018

import numpy as np
import os
import argparse
import lalinference.imrtgr.nrutils as nr
from numpy.lib.recfunctions import append_fields, drop_fields

from automated_postproc_utils import print_Git_status

# Set fits to consider

Mf_fits = ["UIB2016", "HL2016"]

af_fits = ["UIB2016", "HBR2016", "HL2016"]

Lpeak_fits = ["UIB2016", "HL2016"]

# Calculate number of fits

num_Mf_fits = len(Mf_fits)
num_af_fits = len(af_fits)
num_Lpeak_fits = len(Lpeak_fits)

# Set suffix for added samples

suffix = 'evol'

# Set up the parsing

parser = argparse.ArgumentParser(description = 'Output the posteriors on the final mass and spin, radiated mass, and peak luminosity from LALInference posterior samples using a variety of NR fits for these quantities.')
parser.add_argument("--posfile", help = "posterior_samples.dat file to read in", required=True)
parser.add_argument("--aligned", help = "flag for aligned-spin runs", action="store_true")
args = parser.parse_args()

print_Git_status('apply_fits_and_append_samples.py',os.path.realpath(__file__))

# Check that the posterior_samples.dat file exists and the user has write access to it

if not os.path.exists(args.posfile):
            raise ValueError("--posfile='%s' does not exist."%args.posfile)

if not os.access(args.posfile, os.W_OK):
            raise ValueError("You do not have write access to --posfile='%s'. This pipeline modifies that file, so you should copy it to your own directory."%args.posfile)
        
# read the posterior samples for the masses and spins
# N.B.: We read in the detector frame masses so that we can output the detector frame final mass, and also read in the redshift so that we can obtain the source frame final mass and radiated energy from the detector frame masses. The final spin and peak luminosity only depend on the masses through the mass ratio, so we can use the detector frame masses to obtain them with no redshift factors needed, as noted below.
data = np.genfromtxt(args.posfile, dtype=None, names=True)
if ('m1' in data.dtype.names) and ('m2' in data.dtype.names) and ('redshift' in data.dtype.names) and ('a1' in data.dtype.names) and ('a2' in data.dtype.names):
        m1, m2 = data['m1'], data['m2']
        z = data['redshift']
        num_samples = len(m1)
        chi1, chi2 = data['a1'], data['a2']
else:
        raise ValueError("The posterior_samples.dat file is missing at least one of m1, m2, redshift, a1, and a2. Check that you have passed the correct file.")
if 'lambda1' in data.dtype.names or 'lambda2' in data.dtype.names or 'lam_tilde' in data.dtype.names or 'dlam_tilde' in data.dtype.names or 'lambdat' in data.dtype.names or 'dlambdat' in data.dtype.names:
    raise ValueError("There are tidal deformability samples present in the posterior_samples.dat file you passed. The fits this script applies are only applicable to binary black holes.")
if args.aligned:
  if ('tilt1_isco' in data.dtype.names) or ('tilt2_isco' in data.dtype.names) or ('phi12_isco' in data.dtype.names) or ('tilt1' in data.dtype.names) or ('tilt2' in data.dtype.names) or ('phi12' in data.dtype.names):
    raise ValueError("Found spin tilts and phi12, but you are using the --aligned flag. You should not use that flag for precessing runs.")
  else:
    print("Aligned spin computation")
  if ('a1z' in data.dtype.names) and ('a2z' in data.dtype.names):
	  chi1, chi2 = data['a1z'], data['a2z']
  else:
	  raise ValueError("The posterior_samples.dat file is missing at least one of a1z and a2z. Check that you have passed the correct file. This code is not set up to work with nonspinning PE output.")
  tilt1, tilt2 = 0.5*np.pi*(1. - np.sign(chi1)), 0.5*np.pi*(1. - np.sign(chi2)) # Compute tilt angles from components of spins along the orbital angular momentum
  phi12 = np.zeros(num_samples)
  chi1, chi2 = abs(chi1), abs(chi2)
else:
  if ('tilt1_isco' in data.dtype.names) and ('tilt2_isco' in data.dtype.names) and ('phi12_isco' in data.dtype.names):
    print("Precessing spin computation")
    tilt1, tilt2 = data['tilt1_isco'], data['tilt2_isco']
    phi12 = data['phi12_isco']
  elif ('tilt1' in data.dtype.names) and ('tilt2' in data.dtype.names) and ('phi12' in data.dtype.names):
    raise ValueError("This script does not work directly on samples from a precessing run without performing spin evolution first. Please run make_BBH_spin_evolution_and_fit_application_pipeline.py instead.")
  else:
    raise ValueError("At least one of the spin tilts or phi12 is not found--cannot apply precessing expressions. Is this an aligned-spin run? In that case use the --aligned flag.")

if 'mf_evol' in data.dtype.names or 'mf_source_evol' in data.dtype.names or 'af_evol' in data.dtype.names or 'afz_evol' in data.dtype.names or 'e_rad_evol' in data.dtype.names or 'l_peak_evol' in data.dtype.names:
    raise ValueError("At least one of mf_evol, mf_source_evol, af_evol, afz_evol, e_rad_evol, or l_peak_evol is present. The code cannot add evolved final mass and spin, radiated energy, and peak luminosity samples in this case.")

# Apply averaged fits

Mf_evol = nr.bbh_average_fits_precessing(m1, m2, chi1, chi2, tilt1, tilt2, phi12=np.array([0.]), quantity="Mf", fits=Mf_fits) # Final mass computation does not use phi12, so we set it to zero
af_evol = nr.bbh_average_fits_precessing(m1, m2, chi1, chi2, tilt1, tilt2, phi12=phi12, quantity="af", fits=af_fits)
afz_evol = np.zeros_like(af_evol)
for fitname in af_fits:
        afz_evol += nr.bbh_final_spin_projected_spins(m1, m2, chi1, chi2, tilt1, tilt2, fitname)
afz_evol /= len(af_fits)
Lpeak_evol = nr.bbh_average_fits_precessing(m1, m2, chi1, chi2, tilt1, tilt2, phi12=np.array([0.]), quantity="Lpeak", fits=Lpeak_fits) # Peak luminosity computation does not use phi12, so we set it to zero

Mf_evol_source = Mf_evol/(1.+z)

# Calculate radiated energy (source frame)

Erad_evol = (m1 + m2)/(1.+z) - Mf_evol_source

# Append samples
data = append_fields(data, ['mf_%s'%suffix, 'mf_source_%s'%suffix, 'af_%s'%suffix, 'afz_%s'%suffix, 'l_peak_%s'%suffix, 'e_rad_%s'%suffix], [Mf_evol, Mf_evol_source, af_evol, afz_evol, Lpeak_evol, Erad_evol], np.double)

# Remove original af, mf, mf_source, e_rad, and l_peak samples
data = drop_fields(data, ['af', 'mf', 'mf_source', 'e_rad', 'l_peak'])

# Construct header

data_header = data.dtype.names[0]

for k in np.arange(len(data.dtype.names)-1):
  data_header = data_header +'\t ' +data.dtype.names[k+1]

# Save posterior_samples.dat file

np.savetxt(args.posfile, data, header=data_header, delimiter='\t')
