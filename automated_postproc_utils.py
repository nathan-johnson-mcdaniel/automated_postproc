# Functions to print Git status info and compute the orbital velocity from the 2,2 mode GW frequency
# NKJ-M, 5.2018; updated 7.2018

import os, subprocess, pkg_resources

import numpy as np
import scipy

from pylal import git_version

from lal import MTSUN_SI

def print_Git_status(script_name, script_path, output_branch=False):
        separator = '\n' + '-'*20 + '\n'

        print '* LALSuite Git info:\n'

        print git_version.verbose_msg

        print separator

        # Check to see if we are running on an installed version of the scripts

        installed_flag = True

        try:
                package_info = pkg_resources.require("automated_postproc_Mf_af_Erad_Lpeak")[0]
        except:
                installed_flag = False
                
        if installed_flag:
		try:
			installed_path = subprocess.check_output('which %s'%script_name,shell=True)
		except:
			installed_flag = False

		if installed_flag:
			installed_path = installed_path.strip() # Remove newline

			# Check whether the path of the script is the same as we got when running "which script_name" (appropriate for an installation using "setup.py build")
			# or if the expected directory name from a "setup.py install" installation is in the path
			if os.path.samefile(script_path,installed_path) or 'automated_postproc_Mf_af_Erad_Lpeak-%s-py2.7.egg'%package_info.version in script_path:
				print "* automated_postproc_Mf_af_Erad_Lpeak info:\n"
                
				print "Installed from Git hash", package_info.version
			else:
				print "Note: The package automated_postproc_Mf_af_Erad_Lpeak is installed, but you are not running the installed version of %s, but rather %s\n"%(script_name, script_path)

				installed_flag = False
        if not installed_flag:
		git_flag = True

		script_dir = os.path.dirname(os.path.realpath(__file__))

		orig_dir = os.getcwd()

		os.chdir(script_dir)

		try:
			git_hash = subprocess.check_output('git rev-parse HEAD',shell=True)
		except:
			print "WARNING: The version of %s you are running (%s) does not appear to be either an installed version or under Git version control"%(script_name, script_path)

			git_flag = False

		if git_flag:
			print '* automated_postproc Git info:\n'

			print "Script directory:", script_dir

			print

			print "Latest commit"
			print git_hash
			print
			print subprocess.check_output('git status --untracked-files=no',shell=True)

		os.chdir(orig_dir)

        print separator

        print '* numpy and scipy version info:\n'

        print "numpy version", np.version.version
        print "scipy version", scipy.version.version

        print separator

        if output_branch:
                return git_version.branch

def v_orb_from_f22(m1, m2, f22):
        return ((m1+m2)*MTSUN_SI*np.pi*f22)**(1./3) # Note that since f22 is a (2,2 mode) GW frequency, the associated orbital angular velocity is pi*f22
