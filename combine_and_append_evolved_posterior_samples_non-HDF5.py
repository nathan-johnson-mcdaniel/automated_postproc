#!/usr/bin/env python

'''
This script combines the evolved posterior samples and appends them to posterior_samples.dat file.

Usage:
python combine_and_append_evolved_posterior_samples_non-HDF5.py --indir <location of small files> --nfiles <number of input files to combine> --posfile <posterior_samples.dat file to add samples to>
'''
#(C) Anuradha Gupta, 18-03-2016, modified from combine_evolved_posterior_samples.py by NKJ-M, 11.2017, and then modified again by NKJ-M from Anuradha Gupta's combine_and_append_evolved_posterior_samples.py, 5.2018; final modifications by NKJ-M in 7.2018

import numpy as np
import os
import argparse, shutil
from numpy.lib.recfunctions import append_fields

from automated_postproc_utils import print_Git_status

# Set up the parsing
parser = argparse.ArgumentParser(description = 'combining the posterior sample files into one file.')
parser.add_argument("--indir", help = "directory containing small files to combine", required=True)
parser.add_argument("--nfiles", help = "number of files to be combined", required=True)
parser.add_argument("--posfile", help = "input posterior_samples.dat file", required=True)
args = parser.parse_args()

print_Git_status('combine_and_append_evolved_posterior_samples_non-HDF5.py',os.path.realpath(__file__))

indir = args.indir
nfiles = args.nfiles
posfile = args.posfile

prefix = "small_posterior_samples_evolved"

# Creating a list of input file names
filenames = [prefix+'_{0}.dat'.format(i) for i in range(int(nfiles))]

# Setting up blank
tilt1_isco = []
tilt2_isco = []
phi12_isco = []

# Reading in the data from all the files and combining the evolved spin samples
for f in filenames:
    file_data = np.genfromtxt(indir + '/' + f, dtype=None, names=True)
    tilt1_isco = np.append(tilt1_isco, file_data['tilt1'])
    tilt2_isco = np.append(tilt2_isco, file_data['tilt2'])
    phi12_isco = np.append(phi12_isco, file_data['phi12'])

# Read in posterior_samples.dat file
data = np.genfromtxt(posfile, dtype=None, names=True)

# Append the columns to the existing data from the posterior_samples.dat file
data = append_fields(data, ['tilt1_isco', 'tilt2_isco', 'phi12_isco'], [tilt1_isco, tilt2_isco, phi12_isco], np.double)

# Construct header

data_header = data.dtype.names[0]

for k in np.arange(len(data.dtype.names)-1):
    data_header = data_header + '\t ' + data.dtype.names[k+1]

# Write the appended-to samples file (overwriting the sample file that was read in)
np.savetxt(posfile, data, header=data_header, delimiter = '\t')

# Deleting the smaller files created in the process
print "...deleting all the small sample files"
shutil.rmtree(indir)
split_samples_dir = indir.replace('evolved_samples', 'split_samples')
shutil.rmtree(split_samples_dir)
