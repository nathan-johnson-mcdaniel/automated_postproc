#!/usr/bin/env python

'''
Code to read in LALInference posterior samples at each node, evolve the spins up to the ISCO, and output the resulting mass and spin samples. This code takes in the input posterior sample file with name `sometag.nodenum` and outputs `..._nodenum.dat`.

Usage:
python evolve_spins.py --indir <input file directory> --outdir <output directory> --flow = <GW start frequency> --vfinal <final orbital velocity> --nodenum <node num>
'''
# NKJ-M, 02.2016; Anuradha Gupta, 03.2016

import numpy as np
import argparse
from  lalinference.imrtgr import pneqns
import os, sys

from automated_postproc_utils import print_Git_status, v_orb_from_f22

# Settings
dt = 39.65 # steps in time for the integration. This is ~1./5120./MTSUN_SI, since we needed to convert our previous dt (in s) to Msun now that we have removed factors of MTSUN_SI from pneqns.py

# Set up the parsing
parser = argparse.ArgumentParser(description = 'Evolve the LALInference posterior samples of spins')
parser.add_argument("--indir", help = "input file directory", required=True)
parser.add_argument("--outdir", help = "output file directory", required=True)
parser.add_argument("--nodenum", help = "Node number for the condor runs", type=int, required=True)
parser.add_argument("--vfinal", help = "some fiducial value for final orbital velocity", type=str, default="ISCO")
parser.add_argument("--flow", help = "Start GW frequency in Hz", type=float, required=True)
args = parser.parse_args()

print_Git_status('evolve_spins.py',os.path.realpath(__file__))

infile = args.indir+'/small_posterior_samples_%s.dat'%(args.nodenum)

flow = args.flow

if args.vfinal=='ISCO':
    v_final = 6**-0.5
else:
   raise ValueError("v_final is not ISCO")

# read the posterior samples for the masses and spins
data = np.genfromtxt(infile, dtype=None, names=True)
if ('m1' in data.dtype.names) and ('m2' in data.dtype.names):
  m1, m2 = np.atleast_1d(data['m1']), np.atleast_1d(data['m2']) # We use the redshifted masses here, since fref is in the detector frame
else:
  print("FAILURE! Masses not found--there is likely something wrong with the posterior file you input.")
  sys.exit()
if ('a1' in data.dtype.names) and ('a2' in data.dtype.names):
  chi1, chi2 = np.atleast_1d(data['a1']), np.atleast_1d(data['a2'])
else:
  print("FAILURE! No spins found to evolve.")
  sys.exit()
if ('tilt1' in data.dtype.names) and ('tilt2' in data.dtype.names):
  tilt1, tilt2 = np.atleast_1d(data['tilt1']), np.atleast_1d(data['tilt2'])
else:
  print("FAILURE! No tilts found.")
  sys.exit()
if 'phi12' in data.dtype.names:
  phi12 = np.atleast_1d(data['phi12'])
else:
  print("FAILURE! No phi12 found.")
  sys.exit()

# Evolve spins
for i in range(len(m1)):
    v0 = v_orb_from_f22(m1[i],m2[i],flow)
    if np.logical_and(tilt1[i] > 1e-3, tilt2[i] > 1e-3):
        tilt1[i], tilt2[i], phi12[i] = pneqns.find_tilts_and_phi12_at_freq(v0, m1[i], m2[i], chi1[i]*np.sin(tilt1[i]), 0., chi1[i]*np.cos(tilt1[i]), chi2[i]*np.sin(tilt2[i])*np.cos(phi12[i]), chi2[i]*np.sin(tilt2[i])*np.sin(phi12[i]), chi2[i]*np.cos(tilt2[i]), 0., 0., 1., v_final, dt)

outfile = args.outdir+'/small_posterior_samples_evolved_%s.dat'%(args.nodenum)

np.savetxt(outfile, zip(tilt1, tilt2, phi12), header = "tilt1\t tilt2\t phi12", delimiter = "\t")
