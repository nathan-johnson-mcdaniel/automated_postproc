#!/usr/bin/env python

# Create a pipeline for splitting, evolving, and combining samples, and applying the fits
# NKJ-M, 5.2018, based on code by Anuradha Gupta in lalinference_pipe_utils.py, plus bits of lalinference_pipe.py

import numpy as np
import os, subprocess
import argparse
from glue import pipeline
from lalinference.lalinference_pipe_utils import mkdirs

import lalsimulation as lalsim

from automated_postproc_utils import print_Git_status, v_orb_from_f22

# Set up the parsing
parser = argparse.ArgumentParser(description = 'Create pipeline for splitting, evolving, and combining samples, and applying the fits.')
parser.add_argument("--rundir", help = "run directory", required=True)
parser.add_argument("--posfile", help = "input posterior_samples.dat file", required=True)
parser.add_argument("--ntasks", help = "number of tasks to use for parallelization", type=int, required=True)
args = parser.parse_args()

# Output Git status information and check that we're indeed running on the lalinference_o2 branch
LALSuite_branch = print_Git_status('make_BBH_spin_evolution_and_fit_application_pipeline.py',os.path.realpath(__file__), output_branch=True)
if LALSuite_branch != 'lalinference_o2':
    raise ValueError("This script must be run in the lalinference_o2 environment. Your LALSuite installation is from the %s branch."%LALSuite_branch)

# Set accounting group tag
accounting_group_tag = 'ligo.prod.o2.cbc.pe.lalinference'

# Set scripts to execute and check that they're present

script_dir = os.path.dirname(os.path.realpath(__file__)) # Find directory of this script

split_script = os.path.join(script_dir,'split_posterior_samples.py')
evolve_script = os.path.join(script_dir,'evolve_spins.py')
combine_script = os.path.join(script_dir,'combine_and_append_evolved_posterior_samples_non-HDF5.py')
apply_fits_script = os.path.join(script_dir,'apply_fits_and_append_samples.py')

missing_files = False
for file in [split_script, evolve_script, combine_script, apply_fits_script]:
    if not os.path.isfile(file):
        print "%s is missing. Make sure you have a complete checkout of o2-postprocessing/automated_postproc."%file
        missing_files = True
if missing_files:
    raise ValueError("At least one required subsidiary script file is missing. Please rectify this before creating the DAG.")

# Set final velocity
v_final = 'ISCO'

ntasks = args.ntasks
rundir = args.rundir

# Check that the posterior_samples.dat file exists and the user has write access to it

if not os.path.exists(args.posfile):
    raise ValueError("--posfile='%s' does not exist."%args.posfile)

if not os.access(args.posfile, os.W_OK):
    raise ValueError("You do not have write access to --posfile='%s'. This pipeline modifies that file, so you should copy it to your own directory."%args.posfile)
    
# Create rundir if it does not already exist

mkdirs(rundir)

# Find number of cores on machine
condor_status_output = subprocess.check_output('condor_status -total',shell=True)

split_output = condor_status_output.splitlines() # Split on \n

# To get the "Total-Total" entry from the table output by condor_status -total,
# select out final line, split it on whitespace, and select out the second entry from the left
ncores_tot = int(split_output[-1].split()[1])

# Check that ntasks makes sense

if ntasks < 1:
    raise ValueError("ntasks has to be at least 1")
elif ntasks == 1:
    print "WARNING: With ntasks = 1, you aren't parallelizing the spin evolution at all. Please make sure that this is what you want to do.\n"
elif ntasks > 0.25*ncores_tot and ntasks <= ncores_tot:
    print "WARNING: You will be parallelizing over %d cores. This seems rather large, compared to the total number of cores of %d returned by 'condor_status -total'. Please make sure that this is what you want to do.\n"%(ntasks, ncores_tot)
elif ntasks > ncores_tot:
    raise ValueError("You would be parallelizing over %d cores. The total number of cores returned by 'condor_status -total' is only %d. You likely want to reduce ntasks significantly."%(ntasks, ncores_tot))
    
# Read in reference frequency (and check that it is the same for all samples); use the minimum frequency instead for SEOBNRv3, since it is parameterized by the spins at this frequency, not f_ref
data = np.genfromtxt(args.posfile, dtype=None, names=True)
if 'm1' not in data.dtype.names or 'm2' not in data.dtype.names or 'a1' not in data.dtype.names or 'a2' not in data.dtype.names:
    raise ValueError("At least one of m1, m2, a1, or a2 samples are missing in the posterior_samples.dat file you passed. Please check that you have passed the correct file.")
if 'lambda1' in data.dtype.names or 'lambda2' in data.dtype.names or 'lam_tilde' in data.dtype.names or 'dlam_tilde' in data.dtype.names or 'lambdat' in data.dtype.names or 'dlambdat' in data.dtype.names:
    raise ValueError("There are tidal deformability samples present in the posterior_samples.dat file you passed. The fits and spin evolution this pipeline applies are only applicable to binary black holes.")
if 'tilt1' not in data.dtype.names or 'tilt2' not in data.dtype.names or 'phi12' not in data.dtype.names:
    raise ValueError("At least one of tilt1, tilt2, or phi12 samples are missing in the posterior_samples.dat file you passed. Please check that you have passed the posterior_samples.dat file from a run using a precessing waveform.\n\nIf this is an aligned-spin run and you want to add the updated final mass and spin, radiated energy and peak luminosity samples, run\n\napply_fits_and_append_samples --posfile %s --aligned"%args.posfile)
if 'tilt1_isco' in data.dtype.names or 'tilt2_isco' in data.dtype.names or 'phi12_isco' in data.dtype.names:
    raise ValueError("At least one of tilt1_isco, tilt2_isco, or phi12_isco is present. The pipeline is unable to add evolved samples in this case. If you are missing some of these samples, then run on a version of the posterior_samples.dat file without the isco samples present. If you are trying to produce the final mass and spin, radiated energy, and peak luminosity samples, run apply_fits_and_append_samples.py on this file directly.")
if 'mf_evol' in data.dtype.names or 'mf_source_evol' in data.dtype.names or 'af_evol' in data.dtype.names or 'afz_evol' in data.dtype.names or 'e_rad_evol' in data.dtype.names or 'l_peak_evol' in data.dtype.names:
    raise ValueError("At least one of mf_evol, mf_source_evol, af_evol, afz_evol, e_rad_evol, or l_peak_evol is present. The pipeline cannot add evolved final mass and spin, radiated energy, and peak luminosity samples in this case.")
if 'lal_approximant' not in data.dtype.names:
    raise ValueError('lal_approximant is missing--unable to determine which waveform was used to obtain the samples.')

lal_approx_data = data['lal_approximant']

SEOBNRv3_approx_num = lalsim.GetApproximantFromString('SEOBNRv3')

if np.any(data['lal_approximant'] == SEOBNRv3_approx_num):
    if 'flow' not in data.dtype.names:
        raise ValueError("flow not found in input posterior_samples.dat file. Please check that you have passed the correct file.")
    idx_SEOBNRv3 = np.where(data['lal_approximant'] == SEOBNRv3_approx_num)

    idx_non_SEOBNRv3 = np.where(data['lal_approximant'] != SEOBNRv3_approx_num)

    start_freq_SEOBNRv3_data = data['flow'][idx_SEOBNRv3]

    if min(start_freq_SEOBNRv3_data) != max(start_freq_SEOBNRv3_data):
        raise ValueError("Not all SEOBNRv3 samples have the same flow value. The spin evolution code is not set up to deal with such cases.")

    start_freq = start_freq_SEOBNRv3_data[0]

    if len(idx_non_SEOBNRv3[0]) > 0:
        if 'f_ref' not in data.dtype.names:
            raise ValueError("f_ref not found in input posterior_samples.dat file. Please check that you have passed the correct file.")
        start_freq_non_SEOBNRv3_data = data['f_ref'][idx_non_SEOBNRv3]

        if min(start_freq_non_SEOBNRv3_data) != max(start_freq_non_SEOBNRv3_data):
            raise ValueError("Not all non-SEOBNRv3 samples have the same f_ref value. The spin evolution code is not set up to deal with such cases.")
        if start_freq != start_freq_non_SEOBNRv3_data[0]:
            raise ValueError("The f_ref value of the non-SEOBNRv3 samples is not the same as the flow value of the SEOBNRv3 samples. The spin evolution code is not set up to deal with such cases.")
else:
    start_freq_data = data['f_ref']
    if min(start_freq_data) != max(start_freq_data):
        raise ValueError("Not all samples have the same f_ref value. The spin evolution code is not set up to deal with such cases.")
    start_freq = start_freq_data[0]

# Check that none of the samples have a start orbital velocity above the ISCO orbital velocity, since the code is not set up to handle such a case

v0 = v_orb_from_f22(data['m1'],data['m2'],start_freq)

if np.any(v0 > 6.**-0.5):
    raise ValueError("The maximum initial orbital velocity in these samples is %.3f, which is above the ISCO orbital frequency of 0.408. The code is not set up to deal with such a case. If possible, you should rerun with a lower f_ref (or flow for an SEOBNRv3 run)."%max(v0))

# Set up for writing DAG

class LALInferencePipelineDAG(pipeline.CondorDAG):
  def __init__(self):
	# Add log file
	self.daglogfile=os.path.join(rundir,'split_evolve_combine_apply_fits.log')
	pipeline.CondorDAG.__init__(self,self.daglogfile,dax=None)

	self.logdir = os.path.join(rundir, 'log')
	mkdirs(self.logdir)

	self.add_split_evolve_combine_apply_fits_analysis()

	self.dagfilename="split_evolve_combine_apply_fits"
	self.set_dag_file(self.dagfilename)

  def add_split_evolve_combine_apply_fits_analysis(self):
      # for splitting
      self.split_samples_job=SplitSamplesJob('split_samples.sub',self.logdir,dax=None)
      split_samples_node = SplitSamplesNode(self.split_samples_job)
      self.add_node(split_samples_node)

      # for evolving
      self.evolve_samples_job=EvolveSamplesJob('evolve_samples.sub',self.logdir,dax=None)
      evolve_samples_nodes=[]
      for i in range(ntasks):
          evolve_samples_node = EvolveSamplesNode(self.evolve_samples_job)
          evolve_samples_node.set_nodenum(i)
          self.add_node(evolve_samples_node)
          evolve_samples_node.add_split_parent(split_samples_node)
          evolve_samples_nodes.append(evolve_samples_node)

      # for combining
      self.combine_samples_job=CombineSamplesJob('combine_samples.sub',self.logdir,dax=None)
      combine_samples_node = CombineSamplesNode(self.combine_samples_job,parents=evolve_samples_nodes)
      self.add_node(combine_samples_node)

      # for applying fits
      self.apply_fits_job=ApplyFitsJob('apply_fits.sub',self.logdir,dax=None)
      apply_fits_node = ApplyFitsNode(self.apply_fits_job,parent=combine_samples_node)
      self.add_node(apply_fits_node)

class SplitSamplesNode(pipeline.CondorDAGNode):
  """
  Node to run posterior sample splitting into small files for parallelization
  """
  def __init__(self,split_sample_job,posfile=None,parent=None):
      pipeline.CondorDAGNode.__init__(self,split_sample_job)
      self.set_posterior_file(args.posfile)
      if posfile:
          self.set_posterior_file(posfile)

  def set_posterior_file(self,posfile):
      self.add_file_opt('samples',posfile,file_is_output_file=False)
      self.posfile=posfile

class SplitSamplesJob(pipeline.CondorDAGJob,pipeline.AnalysisJob):
  """
  Class for sample splitting jobs
  """
  def __init__(self,submitFile,logdir,dax=False):
      exe=split_script
      mkdirs(os.path.join(rundir, 'split_samples'))
      pipeline.CondorDAGJob.__init__(self,"vanilla",exe)
      pipeline.AnalysisJob.__init__(self,None,dax=dax)
      self.add_condor_cmd('accounting_group', accounting_group_tag)
      self.set_sub_file(submitFile)
      self.set_stdout_file(os.path.join(logdir,'split_samples-$(cluster)-$(process).out'))
      self.set_stderr_file(os.path.join(logdir,'split_samples-$(cluster)-$(process).err'))
      self.add_condor_cmd('getenv','True')
      self.add_opt('nfiles',str(ntasks))
      self.add_opt('outdir',os.path.join(rundir, 'split_samples'))
      
class EvolveSamplesNode(pipeline.CondorDAGNode):
  """
  Node to evolve posterior sample that are split into small files
  """
  def __init__(self,evolve_sample_job,parent=None,nodenum=None):
      pipeline.CondorDAGNode.__init__(self,evolve_sample_job)
      if parent:
          self.add_parent(parent)
      self.nodenum = nodenum

  def add_split_parent(self,splitsamplesnode):
      self.add_parent(splitsamplesnode)

  def set_nodenum(self,nodenum):
      self.add_var_arg('--nodenum %s'%nodenum)

class EvolveSamplesJob(pipeline.CondorDAGJob,pipeline.AnalysisJob):
  """
  Class for evolving the small sample files in parallel
  """
  def __init__(self,submitFile,logdir,dax=False):
      exe=evolve_script
      queue=str(ntasks)
      mkdirs(os.path.join(rundir, 'evolved_samples'))
      pipeline.CondorDAGJob.__init__(self,"vanilla",exe)
      pipeline.AnalysisJob.__init__(self,None,dax=dax)
      self.add_condor_cmd('accounting_group', accounting_group_tag)
      self.set_sub_file(submitFile)
      self.set_stdout_file(os.path.join(logdir,'evolve_samples-$(cluster)-$(process).out'))
      self.set_stderr_file(os.path.join(logdir,'evolve_samples-$(cluster)-$(process).err'))
      self.add_condor_cmd('getenv','True')
      self.add_opt('indir',os.path.join(rundir, 'split_samples'))
      self.add_opt('outdir',os.path.join(rundir, 'evolved_samples'))
      self.add_opt('flow',str(start_freq))
      self.add_opt('vfinal',str(v_final))

class CombineSamplesNode(pipeline.CondorDAGNode):
  """
  Node to combine the small posterior sample that are evolved
  """
  def __init__(self,combine_sample_job,parents=None):
      pipeline.CondorDAGNode.__init__(self,combine_sample_job)
      if parents is not None:
         for parent in parents:
            self.add_parent(parent)

class CombineSamplesJob(pipeline.CondorDAGJob,pipeline.AnalysisJob):
  """
  Class for combining the small evolved sample files
  """
  def __init__(self,submitFile,logdir,dax=False):
      exe=combine_script
      pipeline.CondorDAGJob.__init__(self,"vanilla",exe)
      pipeline.AnalysisJob.__init__(self,None,dax=dax)
      self.add_condor_cmd('accounting_group', accounting_group_tag)
      self.set_sub_file(submitFile)
      self.set_stdout_file(os.path.join(logdir,'combine_samples-$(cluster)-$(process).out'))
      self.set_stderr_file(os.path.join(logdir,'combine_samples-$(cluster)-$(process).err'))
      self.add_condor_cmd('getenv','True')
      self.add_opt('indir',os.path.join(rundir, 'evolved_samples'))
      self.add_opt('nfiles',str(ntasks))
      self.add_opt('posfile',args.posfile)

class ApplyFitsNode(pipeline.CondorDAGNode):
  """
  Node to apply fits on the evolved samples
  """
  def __init__(self,apply_fits_job,parent=None):
      pipeline.CondorDAGNode.__init__(self,apply_fits_job)
      if parent is not None:
          self.add_parent(parent)

class ApplyFitsJob(pipeline.CondorDAGJob,pipeline.AnalysisJob):
  """
  Class for applying fits to the evolved spin samples
  """
  def __init__(self,submitFile,logdir,dax=False):
      exe=apply_fits_script
      pipeline.CondorDAGJob.__init__(self,"vanilla",exe)
      pipeline.AnalysisJob.__init__(self,None,dax=dax)
      self.add_condor_cmd('accounting_group', accounting_group_tag)
      self.set_sub_file(submitFile)
      self.set_stdout_file(os.path.join(logdir,'apply_fits-$(cluster)-$(process).out'))
      self.set_stderr_file(os.path.join(logdir,'apply_fits-$(cluster)-$(process).err'))
      self.add_condor_cmd('getenv','True')
      self.add_opt('posfile',args.posfile)

# Create DAG

dag=LALInferencePipelineDAG()

full_dag_path=os.path.join(rundir, dag.get_dag_file())
dagjob=pipeline.CondorDAGManJob(full_dag_path,dir='.')
dagnode=pipeline.CondorDAGManNode(dagjob)

dag.write_sub_files()
dag.write_dag()
dag.write_script()

# End of program
print('Successfully created DAG file.')

print('To submit the DAG, run:\n\tcondor_submit_dag {0}'.format(dag.get_dag_file()))
