# Setup script to install automated_postproc scripts
# NKJ-M, 7.2018

import os, subprocess

from setuptools import setup

# Get Git version

git_version = subprocess.check_output('git rev-parse HEAD',shell=True)

# Perform setup

setup(
name = 'automated_postproc_Mf_af_Erad_Lpeak',
version = git_version.strip(),
py_modules = ['automated_postproc_utils'],
scripts = ['apply_fits_and_append_samples.py', 'combine_and_append_evolved_posterior_samples_non-HDF5.py', 'evolve_spins.py', 'make_BBH_spin_evolution_and_fit_application_pipeline.py', 'split_posterior_samples.py']
)
