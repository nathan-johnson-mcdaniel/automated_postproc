#!/usr/bin/env python
'''
This script splits a large posterior_sample.dat file into small files.
This is useful in evolving posterior samples in parallel.

Usage:
python split_posterior_samples.py --samples <directory path to the posterior samples>\
      --outdir <directory path where the small files will be created>\
      --nfiles <No of files>
'''
#(C) Bhooshan Gadre, Anuradha Gupta, 06-04-2018

import numpy as np
import argparse
import os

from automated_postproc_utils import print_Git_status

# The following code will create <No of files> files

# Set up the parsing
parser = argparse.ArgumentParser(description = 'splitting the posterior samples files into small files.')
parser.add_argument("--samples", help = "path to the input posterior samples file", required=True)
parser.add_argument("--outdir", help = "directory for output files (default: current directory)", default = ".")
parser.add_argument("--nfiles", help = "Number of smaller files", type=int, required=True)
args = parser.parse_args()

print_Git_status('split_posterior_samples.py',os.path.realpath(__file__))

no_of_files = args.nfiles
datfile = args.samples
tag = 'small_posterior_samples'

# Open the data files to read in the total number of lines
with open(datfile) as f:
    heads = f.readline()

data = np.loadtxt(datfile, skiprows=1)
data_split = np.array_split(data, no_of_files, axis=0)

# Writing in the different files
for i in range(no_of_files):
    of = '{0}_{1}.dat'.format(tag, i)  # Output file name can be changed as one wishes
    np.savetxt(os.path.join(args.outdir,of), data_split[i], header=heads.rstrip('\n'), comments='')
